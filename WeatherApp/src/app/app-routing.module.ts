import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CompWeeklyweatherComponent } from './components/comp-weeklyweather/comp-weeklyweather.component';
import { CompTodaysweatherComponent } from './components/comp-todaysweather/comp-todaysweather.component';
import { CompDashboardComponent } from './components/comp-dashboard/comp-dashboard.component';

const routes: Routes = [
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  { path: 'dashboard', component: CompDashboardComponent },
  { path: 'todays/:id', component: CompTodaysweatherComponent },
  { path: 'weekly', component: CompWeeklyweatherComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}