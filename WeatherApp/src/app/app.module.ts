import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { CompWeeklyweatherComponent } from './components/comp-weeklyweather/comp-weeklyweather.component';
import { CompTodaysweatherComponent } from './components/comp-todaysweather/comp-todaysweather.component';

// Manually Added Modules
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { CompDashboardComponent } from './components/comp-dashboard/comp-dashboard.component';

@NgModule({
  declarations: [
    //Components
    AppComponent,
    CompWeeklyweatherComponent,
    CompTodaysweatherComponent,
    CompDashboardComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
