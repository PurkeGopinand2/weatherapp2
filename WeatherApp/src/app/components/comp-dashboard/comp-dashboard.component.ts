import { Component, OnInit } from '@angular/core';
// Models
import { weathermodel } from '../../models/weathermodel';
// Controllers
import { GetWeaklyWeather } from '../../controllers/weathercontroller';
// Service
import { WeatherService } from '../../services/weather.service'; 

@Component({
  selector: 'app-comp-dashboard',
  templateUrl: './comp-dashboard.component.html',
  styleUrls: ['./comp-dashboard.component.css']
})
export class CompDashboardComponent implements OnInit {
  days: weathermodel[] = [];

  constructor(private weatherService: WeatherService) { }

  ngOnInit(): void {
    this.getWeeklyWeather();
  }

  getWeeklyWeather(): void {
    this.weatherService.GetWeaklyWeather()
      .subscribe(days => this.days = GetWeaklyWeather.slice(1,2));
  }
}
