import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CompTodaysweatherComponent } from './comp-todaysweather.component';

describe('CompTodaysweatherComponent', () => {
  let component: CompTodaysweatherComponent;
  let fixture: ComponentFixture<CompTodaysweatherComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CompTodaysweatherComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CompTodaysweatherComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
