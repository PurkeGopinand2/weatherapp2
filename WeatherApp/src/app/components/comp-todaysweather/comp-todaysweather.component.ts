import { Component, OnInit, Input } from '@angular/core';

// Models
import { weathermodel } from '../../models/weathermodel';

@Component({
  selector: 'app-comp-todaysweather',
  templateUrl: './comp-todaysweather.component.html',
  styleUrls: ['./comp-todaysweather.component.css']
})
export class CompTodaysweatherComponent implements OnInit {
  @Input() weathermodel: weathermodel;

  constructor() { }

  ngOnInit(): void {
  }

}
