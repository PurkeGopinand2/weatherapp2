import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CompWeeklyweatherComponent } from './comp-weeklyweather.component';

describe('CompWeeklyweatherComponent', () => {
  let component: CompWeeklyweatherComponent;
  let fixture: ComponentFixture<CompWeeklyweatherComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CompWeeklyweatherComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CompWeeklyweatherComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
