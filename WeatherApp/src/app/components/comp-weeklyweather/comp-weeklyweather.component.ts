import { Component, OnInit } from '@angular/core';

// Models
import { weathermodel } from '../../models/weathermodel';
// Controllers
import { GetWeaklyWeather } from '../../controllers/weathercontroller';

@Component({
  selector: 'app-comp-weeklyweather',
  templateUrl: './comp-weeklyweather.component.html',
  styleUrls: ['./comp-weeklyweather.component.css']
})
export class CompWeeklyweatherComponent implements OnInit {

  WeaklyWeathers = GetWeaklyWeather;
  selectedDay: weathermodel

  constructor() { }

  ngOnInit(): void {
  }

  onSelect(day: weathermodel): void {
    this.selectedDay = day;
  }

}
