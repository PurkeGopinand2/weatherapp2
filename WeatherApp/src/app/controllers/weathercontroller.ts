// Models
import { weathermodel } from '../models/weathermodel'

export const GetWeaklyWeather: weathermodel[] = [
  { id: 1, day: 'Sunday', Temperature: '21', WeatherCategory: 'Humid', WeatherUnit: 'C' },
  { id: 2, day: 'Monday', Temperature: '22', WeatherCategory: 'Humid', WeatherUnit: 'C' },
  { id: 3, day: 'Tuesday', Temperature: '23', WeatherCategory: 'Windy', WeatherUnit: 'C' },
  { id: 4, day: 'Wednesday', Temperature: '24', WeatherCategory: 'Windy', WeatherUnit: 'C' },
  { id: 5, day: 'Thursday', Temperature: '25', WeatherCategory: 'Pressure', WeatherUnit: 'C' },
  { id: 6, day: 'Friday', Temperature: '26', WeatherCategory: 'Cloudy', WeatherUnit: 'C' },
  { id: 7, day: 'Saturday', Temperature: '28', WeatherCategory: 'Cloudy', WeatherUnit: 'C' },
];