export interface weathermodel {
    id: number;
    day: string;
    Temperature: string;
    WeatherCategory: string;
    WeatherUnit: string;
  }