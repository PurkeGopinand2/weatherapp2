import { Injectable } from '@angular/core';

//Manually Added Service 
import { Observable, of } from 'rxjs';
// Models
import { weathermodel } from '../models/weathermodel';
// Controllers
import { GetWeaklyWeather } from '../controllers/weathercontroller';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  constructor() { 
  }

  GetWeaklyWeather(): Observable<weathermodel[] > {
    return of(GetWeaklyWeather);
  }

  GetWeaklyWeatherById(id: number): Observable<weathermodel > {
    return of(GetWeaklyWeather.find(hero => hero.id === id));
  }
}
